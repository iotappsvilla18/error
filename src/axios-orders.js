import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://reactburger-c6738.firebaseio.com/'
});

export default instance;